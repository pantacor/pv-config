TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := riscv

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/riscv64-lp64d--musl--stable-2024.05-1/bin/riscv64-linux-

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TARGET_VENDOR_DIR)/skel

TARGET_LINUX_DEVICE_TREE := starfive/jh7110-starfive-visionfive-2-v1.3b.dtb

LINUX_CONFIG_TARGET := starfive_visionfive2_defconfig
TARGET_LINUX_CONFIG_MERGE_FILES := yes

