#!/bin/bash

dd if=${TOP_DIR}/vendor/${PV_TARGET}/SD/BPI-R2-HEAD440-0k.img of=$tmpimg conv=notrunc
dd if=${TOP_DIR}/vendor/${PV_TARGET}/SD/BPI-R2-HEAD1-512b.img of=$tmpimg bs=512 seek=1 conv=notrunc
gunzip -c ${TOP_DIR}/vendor/${PV_TARGET}/SD/BPI-R2-720P-2k.img.gz | dd of=$tmpimg bs=1024 seek=2 conv=notrunc

