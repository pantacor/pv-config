#!/bin/bash
IN_DIR="$1"
OUT_FILE="$2"

if [ -d "${IN_DIR}" ]; then
	dd if=${IN_DIR}/BPI-R2-HEAD440-0k.img of=$OUT_FILE conv=notrunc
	dd if=${IN_DIR}/BPI-R2-HEAD1-512b.img of=$OUT_FILE bs=512 seek=1 conv=notrunc
	gunzip -c ${IN_DIR}/BPI-R2-preloader-DDR1600-20190722-2k.img.gz | dd of=$OUT_FILE bs=1024 seek=2 conv=notrunc
fi

