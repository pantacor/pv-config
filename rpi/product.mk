TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := arm
TARGET_ARCH_v7 := arm
TARGET_ARCH_v8 := aarch64

TARGET_LINUX_EXTRA := v7


TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/armv7-eabihf--musl--stable-2024.05-1/bin/arm-linux-
TARGET_LINUX_CROSS_v8 := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/aarch64--musl--stable-2024.05-1/bin/aarch64-linux-
TARGET_CROSS_CROSS_v7 := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/armv7-eabihf--musl--stable-2024.05-1/bin/arm-linux-

TARGET_GLOBAL_CFLAGS += -march=armv6 -mfloat-abi=hard -mfpu=vfp
TARGET_DEFAULT_ARM_MODE = arm

#TARGET_LINUX_IMAGE := uImage

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TARGET_VENDOR_DIR)/skel

TARGET_UBI_IMAGE_OPTIONS := \
       --mkubifs="-m 1 -e 65408 -c 247 -x favor_lzo -F" \
       --ubinize="-p 0x10000 -m 1 $(TARGET_CONFIG_DIR)/ubinize.config"

LINUX_CONFIG_TARGET := bcm2711_defconfig
LINUX_CONFIG_TARGET_v7l := bcm2711_defconfig
LINUX_CONFIG_TARGET_v7 := bcm2709_defconfig
LINUX_CONFIG_TARGET_v6 := bcmrpi_defconfig
LINUX_CONFIG_TARGET_2712 := bcm2712_defconfig
TARGET_LINUX_CONFIG_MERGE_FILES := yes

INITRD_CONFIG := trail.config

TARGET_LINUX_INSTALL_DEVICE_TREE := 1

TARGET_LINUX_IMAGE_TYPE := "rpiboot-ab"

TARGET_OS_KERNELS := kernel kernel-v7

