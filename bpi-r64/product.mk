TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := aarch64
TARGET_UBOOT_ARCH := arm

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/aarch64--musl--stable-2024.05-1/bin/aarch64-linux-
TARGET_UBOOT_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/aarch64--musl--stable-2024.05-1/bin/aarch64-linux-
TARGET_LINUX_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/aarch64--musl--stable-2024.05-1/bin/aarch64-linux-

TARGET_GLOBAL_CFLAGS += -march=armv8-a

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TARGET_VENDOR_DIR)/skel

TARGET_LINUX_DEVICE_TREE :=
TARGET_LINUX_DEVICE_TREE_NAMES := \
	mediatek/mt7622-bananapi-bpi-r64.dtb \
	mediatek/mt7622-rfb1.dtb \
	$(NULL)	

LINUX_CONFIG_TARGET := defconfig
TARGET_LINUX_CONFIG_MERGE_FILES := yes

