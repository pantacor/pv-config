#!/bin/bash

IN_DIR="$1"
OUT_FILE="$2"

if [ -d "${IN_DIR}" ]; then
	dd if=${IN_DIR}/MLO of=$OUT_FILE conv=notrunc bs=512 seek=256 count=256
	dd if=${IN_DIR}/u-boot.img of=$OUT_FILE bs=512 seek=768 count=1024 conv=notrunc
fi
