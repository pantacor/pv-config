TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := mips

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/mips32--musl--stable-2024.05-1/bin/mips-linux-

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TARGET_VENDOR_DIR)/skel

