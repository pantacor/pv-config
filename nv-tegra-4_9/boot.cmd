pv_mmcdev=${devnum}
pv_mmcboot=${distro_bootpart}
pv_mmcdata=${distro_bootpart}
envloadaddr=${pxefile_addr_r}

pv_baseargs="root=/dev/ram rootfstype=ramfs init=/init pv_storage.fstype=ext4"
if test "${devtype}" = "mmc"; then
	pv_baseargs="${pv_baseargs} pv_storage.device=/dev/mmcblk0p1"
elif test "${devtype}" = "usb"; then
	pv_baseargs="${pv_baseargs} pv_storage.device=/dev/sda1"
fi

echo ENVLOAD TO: ${envloadaddr}
if test -z "${localargs}"; then
	if load ${devtype} ${devnum}:${distro_bootpart} ${loadaddr} /pvoemro.txt; then
		env import ${loadaddr} ${filesize}
		echo ENVIMPORT with configargs: ${configargs}
	fi
fi

echo Load uboot.txt
load ${devtype} ${pv_mmcdev}:${pv_mmcdata} ${envloadaddr} /boot/uboot.txt; uboot_txt_size=${filesize}

echo Loading pantavisor uboot.txt
setenv pv_try; env import ${envloadaddr} ${uboot_txt_size}
v_pv_try=$pv_try
v_pv_rev=$pv_rev
v_pv_trying=$pv_trying
env delete -f pv_try
env delete -f pv_rev
env delete -f pv_trying

echo
if test -n "$v_pv_try"; then
	if test -n "${v_pv_trying}" && test ${v_pv_trying} = ${v_pv_try}; then
		echo Pantavisor boots checkpoint revision ${v_pv_rev} after failed try-boot of revision: ${v_pv_try}
		rev=${v_pv_rev}
		saveenv
		boot_rev=${rev}
	else
		echo Pantavisor boots try-boot revision: ${v_pv_try}
		setenv pv_trying ${v_pv_try}
		saveenv
		boot_rev=${pv_trying}
	fi
else
	echo Pantavisor boots revision: ${pv_rev}
	boot_rev=${v_pv_rev}
fi

echo Pantavisor loading FDT at ${fdt_addr}
fdt addr ${fdt_addr}
fdt get value fdtbootargs /chosen bootargs

echo "load ${devtype} ${pv_mmcdev}:${pv_mmcdata} ${fdt_addr_r} /trails/${boot_rev}/.pv/pv-fdt.dtb"
load ${devtype} ${pv_mmcdev}:${pv_mmcdata} ${fdt_addr_r} /trails/${boot_rev}/.pv/pv-fdt.dtb
echo Pantavisor bootargs: "${fdtbootargs} ${pv_baseargs} pv_try=${v_pv_try} pv_rev=${boot_rev} panic=2 ${configargs} ${localargs}"
setenv bootargs "${fdtbootargs} ${pv_baseargs} pv_try=${v_pv_try} pv_rev=${boot_rev} panic=2 ${configargs} ${localargs}"
echo Pantavisor kernel load: load ${devtype} ${pv_mmcdev}:${pv_mmcdata} ${kernel_addr_r} /trails/${boot_rev}/.pv/pv-kernel.img
load ${devtype} ${pv_mmcdev}:${pv_mmcdata} ${kernel_addr_r} /trails/${boot_rev}/.pv/pv-kernel.img
echo Pantavisor initrd load: load ${devtype} ${pv_mmcdev}:${pv_mmcdata} ${ramdisk_addr_r} /trails/${boot_rev}/.pv/pv-initrd.img
load ${devtype} ${pv_mmcdev}:${pv_mmcdata} ${ramdisk_addr_r} /trails/${boot_rev}/.pv/pv-initrd.img
rd_size=${filesize}
#setexpr rd_offset ${ramdisk_addr_r} + ${rd_size}
#setenv i 0
#while load ${devtype} ${pv_mmcdev}:${pv_mmcdata} ${rd_offset} /trails/${boot_rev}/.pv/pv-initrd.img.${i}; do
#	echo Pantavisor initrd addon loaded: load ${devtype} ${mmcdev}:${mmcdata} ${rd_offset} /trails/${boot_rev}/.pv/pv-initrd.img.${i}
#	setexpr i ${i} + 1
#	setexpr rd_size ${rd_size} + ${filesize}
#	setexpr rd_offset ${rd_offset} + ${filesize}
#done
echo "Pantavisor go... : booti ${kernel_addr_r} ${ramdisk_addr_r}:${rd_size} ${fdt_addr_r}"
booti ${kernel_addr_r} ${ramdisk_addr_r}:${rd_size} ${fdt_addr}
echo "Failed to boot step, rebooting"; sleep 5; reset

