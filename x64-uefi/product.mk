TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := x64

TARGET_LINUX_IMAGE := bzImage

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/x86-64--musl--stable-2024.05-1/bin/x86_64-linux-

#HOST_CPP = x86_64-linux-gnu-gcc-6 -E
HOST_CPP = gcc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

LINUX_CONFIG_TARGET := defconfig
TARGET_LINUX_CONFIG_MERGE_FILES := yes

TARGET_SKEL_DIRS := $(TARGET_VENDOR_DIR)/skel

ifdef PANTAVISOR_DISKCRYPT
TARGET_SKEL_DIRS += $(TOP_DIR)/vendor/x64-uefi/skels/diskcrypt
endif
