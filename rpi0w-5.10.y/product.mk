TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := arm

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/armv7-eabihf--musl--stable-2024.05-1/bin/arm-linux-
TARGET_UBOOT_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/armv7-eabihf--glibc--stable-2024.05-1/bin/arm-linux-
TARGET_LINUX_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/armv7-eabihf--glibc--stable-2024.05-1/bin/arm-linux-

TARGET_GLOBAL_CFLAGS += -march=armv6 -mfloat-abi=hard -mfpu=vfp
TARGET_DEFAULT_ARM_MODE = arm

TARGET_LINUX_IMAGE := uImage

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TARGET_VENDOR_DIR)/skel

LINUX_CONFIG_TARGET := bcmrpi_defconfig
TARGET_LINUX_CONFIG_MERGE_FILES := yes
