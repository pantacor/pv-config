TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := aarch64

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/aarch64--musl--stable-2024.05-1/bin/aarch64-linux-
#TARGET_UBOOT_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/arm-linux-gnueabihf/bin/arm-linux-gnueabihf-
#TARGET_LINUX_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/arm-linux-gnueabihf/bin/arm-linux-gnueabihf-

#TARGET_GLOBAL_CFLAGS += -march=armv8-a

#TARGET_LINUX_IMAGE := uImage

HOST_CPP = cc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TARGET_VENDOR_DIR)/skel

TARGET_UBI_IMAGE_OPTIONS := \
       --mkubifs="-m 1 -e 65408 -c 247 -x favor_lzo -F" \
       --ubinize="-p 0x10000 -m 1 $(TARGET_CONFIG_DIR)/ubinize.config"

LINUX_CONFIG_TARGET := bcm2711_defconfig
TARGET_LINUX_CONFIG_MERGE_FILES := yes

INITRD_CONFIG := trail.config

TARGET_LINUX_INSTALL_DEVICE_TREE := 1

TARGET_LINUX_IMAGE_TYPE := "rpiboot-ab"

