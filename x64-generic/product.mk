TARGET_OS := linux
TARGET_OS_FLAVOUR := generic
TARGET_LIBC := eglibc

TARGET_ARCH := x64

TARGET_CROSS := $(ALCHEMY_WORKSPACE_DIR)/prebuilt/x86-64--musl--stable-2024.05-1/bin/x86_64-linux-

HOST_CPP = gcc -E

TARGET_IMAGE_FORMAT := cpio
TARGET_FINAL_MODE := firmware

TARGET_SKEL_DIRS := $(TOP_DIR)/trail/x64-generic/config/
