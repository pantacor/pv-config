#!/bin/bash

OUT_DIR="$1"
ARTIFACT_OUT_DIR="${OUT_DIR}/tezi"
bootbasename=pantavisor.$subtarget.bootfs.tar.gz
mkdir -p ${ARTIFACT_OUT_DIR}
cp -rvf ${TOP_DIR}/vendor/${PVBUILD_TARGET}/tezi/* ${ARTIFACT_OUT_DIR}/
# we keep the original boot.scr in top level dir but want our own boot.scr in the
# boot tarball
[ -f ${ARTIFACT_OUT_DIR}/boot.scr ] && cp -f ${ARTIFACT_OUT_DIR}/boot.scr ${ARTIFACT_OUT_DIR}/boot.scr.bak
cp -f $boot_scr ${ARTIFACT_OUT_DIR}/boot.scr

# for tezi we just need the nice boot.scr as defined on target for bootfs
tar -C ${ARTIFACT_OUT_DIR}/ -cvzf ${ARTIFACT_OUT_DIR}/$bootbasename boot.scr
name_s=`du -hsb ${ARTIFACT_OUT_DIR}/boot.scr | awk '{ print $1}'`
bootbasename_sM=`echo "scale=5; $name_s / 1024 / 1024" | bc -l | sed 's/^\./0./'`

# bring back the original tezi one...
rm -f ${ARTIFACT_OUT_DIR}/boot.scr
[ -f ${ARTIFACT_OUT_DIR}/boot.scr.bak ] && cp -f ${ARTIFACT_OUT_DIR}/boot.scr.bak ${ARTIFACT_OUT_DIR}/boot.scr
rm -f ${ARTIFACT_OUT_DIR}/boot.scr.bak

rootbasename=pantavisor.$subtarget.ota.tar.gz

# for tezi we just need the nice boot.scr as defined on target for bootfs
tar -C ${ALCHEMY_TARGET_OUT}/trail/final/ -cvzf ${ARTIFACT_OUT_DIR}/$rootbasename .
name_s=`du -hsb ${ALCHEMY_TARGET_OUT}/trail/final/ | awk '{ print $1}'`
rootbasename_sM=`echo "scale=5; $name_s / 1024 / 1024" | bc -l | sed 's/^\./0./'`

sed -i "s/%BOOTBASENAME%/$bootbasename/" ${ARTIFACT_OUT_DIR}/image.*.json
sed -i "s/%BOOTBASENAME_SIZEM%/$bootbasename_sM/" ${ARTIFACT_OUT_DIR}/image.*.json
sed -i "s/%ROOTBASENAME%/$rootbasename/" ${ARTIFACT_OUT_DIR}/image.*.json
sed -i "s/%ROOTBASENAME_SIZEM%/$rootbasename_sM/" ${ARTIFACT_OUT_DIR}/image.*.json
sed -i "s/%RELEASEDATE%/`date +%Y-%m-%d`/" ${ARTIFACT_OUT_DIR}/image.*.json

tar -cvzf $OUT_DIR/${TARGET}-pv-tezi.tar.gz -C ${ARTIFACT_OUT_DIR}/ .

echo "TEZI tarball is here: ${OUT_DIR}/${TARGET}-pv-tezi.tar.gz"
echo "Have fun!"

